#!/usr/bin/env php
<?php

function checkDirectory($dir = '.'){
    if(!file_exists($dir)){
        throw new Exception('There is no such directory.');
    }
    if(!is_readable($dir)){
        throw new Exception('You have no rights to read this directory.');
    }
    if(!is_dir($dir)){
        throw new Exception('Path to directory expected to be a path to existing directory with granted access.');
    }

    static $openedFilesCount = 0;
    static $sum = 0;
    $files = scandir($dir);
    foreach ($files as $file){
        $fullPath = $dir . DIRECTORY_SEPARATOR . $file;
        if($file == '.' || $file == '..'){
            continue;
        }
        if (is_dir($fullPath)){
            checkDirectory($fullPath);
        }
        if($file == 'count'){
            $openedFilesCount += 1;
            $data = file_get_contents($fullPath);
            $sum += intval($data);
        }
    }

    return [$openedFilesCount, $sum];
}

$dir = '.';
$valueOnly = false;

foreach ($argv as $index => $arg){
    if (!$index)
        continue;
    if($arg == '-s'){
        $valueOnly = true;
    } else {
        $dir = $arg;
    }
}

try{
    list($instanceCount, $sum) = checkDirectory($dir);
} catch (Exception $e) {
    echo $e->getMessage() . "\n";
    exit;
}

if($valueOnly){
    echo($sum);
    exit;
}
$instanceCount = $instanceCount ? $instanceCount : 'No';
echo "$instanceCount 'count' files found, $sum sum of numbers from files. \n";
if($dir == '.'){
    echo "Hint: script may be used with directory path as argument like \"count.php ..\" and '-s' to display sum only.\n";
}
